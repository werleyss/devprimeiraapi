﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DevPrimeiraApi.Models
{
    public class Fornecedor
    {
        [Key]
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Documento { get; set; }
        public bool Ativo { get; set; }
    }
}