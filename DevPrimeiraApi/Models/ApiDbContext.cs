﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;

namespace DevPrimeiraApi.Models
{
    public class ApiDbContext : DbContext
    {
        public ApiDbContext(DbContextOptions<ApiDbContext> options) : base(options)
        {

        }
        public DbSet<Fornecedor> Fornecedores { get; set; }

    }

    
}